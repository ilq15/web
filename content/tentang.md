---
title: "Tentang"
title2: "Yayasan Indonesia Learning Quran"
date: 2019-08-06T10:52:03+07:00
draft: false
image :
description: ""
tags: ["tentang"]
layout: "tentang"
---

Sebuah fakta mengejutkan menyatakan bahwa 53.57% kaum muslim di Indonesia tidak bisa membaca Al-Quran (data sumber BPS tahun 2018).


Berangkat dari keprihatinan dan harapan akan kebaikan negeri ini, ILearning Quran Foundation menggagas sebuah mimpi besar melakukan sebuah gerakan massal untuk membantu masyarakat muslim Indonesia agar bisa membaca Al-Quran.


Gerakan ini hadir dengan memberikan pelatihan tidak berbayar kepada masyarakat dengan metode pembelajaran yang dirancang khusus untuk memudahkan peserta dalam belajar membaca Al-Quran. Dengan tujuan semakin banyak orang dari berbagai kalangan yang mampu membaca Al-Quran dan memudahkan orang-orang yang belum bisa membaca untuk selalu dekat dengan Quran sebagai bentuk dukungan terhadap gerakan menuju Indonesia Bebas Buta Huruf Al-Quran.