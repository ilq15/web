---
title: "Pelatihan Kepengurusan Jenazah"
date: 2019-08-06T10:52:03+07:00
draft: false
image : pjenazah.png
description: ""
tags: ["pengurusanjenazah"]
layout: "pengurusanjenazah"
---

Pelatihan kepengurusan jenazah diperuntukan untuk ikhwan/akhwat fillah yang ingin belajar bagaimana tata cara untuk mengurus jenazah dari mulai
memandikan, menyolatkan dan hal - hal sebagainya dalam syariat Islam dengan baik dan benar dengan ketentuan yang telah Rasulullah Shalalahu Alaihi Wassallam.

Dengan tenaga trainner professional, ikhwan/akhwat fillah akan diberikan materi yang layak sesuai dengan kebutuhan ikhwan/akhwat fillah dalam
mengurus jenazah sesuai dengan syariat Islam.

Tunggu apa lagi. Yu gabung bersama kami.