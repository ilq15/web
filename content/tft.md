---
title: "Training For Trainer Indonesia Learning Quran"
date: 2019-08-06T10:52:03+07:00
draft: false
image : tft.png
description: ""
tags: ["tft"]
layout: "tft"
---

Untuk ikhwan/akhwat fillah yang memiliki keinginan untuk bisa mengajarkan Al Quran kepada orang banyak, Yayasan Indonesia Learning Quran
memberikan kesempatan yang luas untuk ikhwan/akhwat fillah untuk bisa menjadi Trainner Professional di bidangnnya dengan mengikuti 
pelatihan Training For Trainer Pengajar Al Quran dengan menggunakan metode Indonesia Learning Quran.

Materi yang akan didapat : 

	- Materi Pembelajaran Al Quran Metode Indonesia Learning Quran
	- Public Speaking
	- Menjadi khotib
	- dan masih banyak lagi
	
Jadi, bukan hanya menjadi seorang peserta saja. Ikhwan/akhwat fillah pun bisa banget loh menjadi seorang pengajar professional
dengan mengikuti Training For Trainner di Indonesia Learning Quran. 

Yu, tunggu apa lagi. Gabung bersama kami dan rasakan kemudahan dalam mengajarkan membaca Al Quran untuk orang banyak dan 
jadilah manfaat untuk Seluruh Umat Muslim.

*Pendaftaran TfT di tutup*

Catatan : Program ini dikhususkan untuk yang sudah masuk Tahsin Tilawah Quran. Untuk yang dari 0 Sabar dulu yah :)