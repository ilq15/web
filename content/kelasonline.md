---
title: "Kelas Online Belajar dari 0"
date: 2019-08-06T10:52:03+07:00
draft: false
image : online.png
description: ""
tags: ["kelasonline"]
layout: "kelasonline"
---

Untuk ikhwan/akhwat fillah yang merasa ga punya waktu atau sibuk untuk belajar Quran, tapi pingin banget nih belajar
untuk memperbaiki bacaan Tilawah Qurannya. Sekarang kan jamannya serba online, belajar Quran pun bisa Online ko.

Bisa gituh ? Bisa banget pastinya. Yayasan Indonesia Learning Quran pun membuka kelas online untuk para mahasiwa, pekerja,
wiraswasta, atau orang - orang tua yang sibuk dan tidak bisa pergi ke kajian Quran untuk memperbaiki bacaan Tilawah
Al Qurannya. 

Cara nya mudah sekali. Kita menggunakan media Aplikasi Whatsapp, dan akan dibagi - bagi menjadi beberapa kelompok kelas,
diataranya : 

	- Kelas Dasar
	- Kelas Lanjutan
	- Kelas Mahir
	
Untuk materi yang dibawakan pun dengan menggunakan metode Indonesia Learning Quran yang mudah, simpel dan menyenangkan 
tentunya. Dengan dimentori oleh mentor - mentor yang tentunya mahir di bidangnnya Insyaa Allah. 

Yu tunggu apa lagi. Gabung bersama kami. 