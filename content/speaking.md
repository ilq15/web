---
title: "Pelatihan Public Speaking"
date: 2019-08-06T10:52:03+07:00
draft: false
image : speaking.png
description: ""
tags: ["speaking"]
layout: "speaking"
---

Sebagai seorang pengajar / mentor, kita dituntut untuk bisa berbicara di depan banyak orang dengan professional. Namun, bagaimana
jika kita tidak terbiasa untuk berbicara di halayak umum dengan tenang? Masih menyimpan banyak grogi, kehabisan kata - kata, 
dan masih banyak lagi kendala lainnya dalam hal menyampaikan sesuatu di depan umum. 

Nah, untuk itu Yayasan Indonesia Learning Quran membuka pelatihan Public Speaking untuk ikhwan/akhwat fillah pengajar atau
yang belum menjadi pengajar sama sekali untuk bisa bicara di depan banyak umum dengan berbagai teknik Public Speaking 
yang membuat ikhwan/akhwat fillah semua menjadi terlihat lebih tenang, enjoy dan professional ketika berbicara di depan banyak 
orang. 

Tentunya, dengan Master Trainner yang Professional di bidangnya ikhwan/akhwat fillah akan dimanjakan dengan materi 
Public Speaking yang seru dan mengasyikan loh. Yu, tunggu apa lagi. Gabung bersama Indonesia Learning Quran untuk 
menjadi umat terbaik yang menolong agama Allah lewat kegiatan - kegiatan yang positif. 